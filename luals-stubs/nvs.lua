-- SPDX-FileCopyrightText: 2023 jacqueline <me@jacqueline.id.au>
--
-- SPDX-License-Identifier: GPL-3.0-only

--- @meta

--- @class nvs
local nvs = {}

--- Saves current nvs
function nvs.write() end

return nvs
